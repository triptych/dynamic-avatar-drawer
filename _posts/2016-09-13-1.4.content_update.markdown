---
layout: post
title:  "1.4 Content Summary (shirts, shorts, hair)"
date:   2016-09-13
categories: release
---
Some pictures to summarize new content since 1.0 release.

## Shirts and Shorts.
Parameterized generation of clothing templates by mixing and matching
Clothing Parts allows pretty much infinite combinations (combinatorial).

<div class="parent" style="height:1000px"><img src="http://i.imgur.com/Y83sh1i.gif" class="child"></div>
<div class="parent" style="height:1000px"><img src="http://i.imgur.com/wXa8GKh.gif" class="child"></div>


# New hairstyles 

![Curly tail](http://i.imgur.com/wNKYEvv.gif)

![Straight](http://i.imgur.com/g81t7jL.gif)
![Hime cut](http://i.imgur.com/xU1unEQ.gif)

![Hime curl](http://i.imgur.com/N2DqXAK.gif)
![High side tail](http://i.imgur.com/4mLaG18.gif)

## Interactive code generation in tester/playground

Generating the code on the fly will make it much easier for people to
learn how to use the tool, as well as to know what parameters are
available if they don't want to consult the reference.

<div class="parent" style="height:1000px"><img src="http://i.imgur.com/jqtuUXC.gif" class="child"></div>

Annotations and units for dimensions and modifiers
![Annotations and units for dimensions and modifiers](http://i.imgur.com/q8rQrpd.png)
