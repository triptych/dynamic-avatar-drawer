import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Layer} from "../util/canvas";
import {
    //simpleQuadratic,
    drawPoints,
    extractPoint,
    reflect,
    breakPoint, splitCurve, simpleQuadratic, adjust, drawCircle, interpolateCurve
} from "drawpoint/dist-esm";
import {Part} from "../parts/part";
import {connectEndPoints, dist, Location} from "..";

export class AnkletPart extends ClothingPart {
    constructor(...data) {
        super({
                layer        : Layer.FRONT,
                loc          : `+${Location.LEG}`,
                /**
                 * Where along the leg (0 is ankle, 1 is knee) to start the bottom
                 */
                startAlongLeg: 0,
                aboveParts   : [`parts ${Location.LEG}`],
            },
            ...data);
    }
}

export class ChainAnkletPart extends AnkletPart {
    constructor(...data) {
        super({
            startAlongLeg: 0.28,
            chainWidth   : 2,
            chainDash    : [3, 7],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        let sp = splitCurve(this.startAlongLeg, ex.ankle.in, ex.calf.in);
        const left = sp.right.p1;
        const query = {x: null, y: left.y};
        const [right] = interpolateCurve(ex.calf.out, ex.ankle.out, query);

        Clothes.simpleStrokeFill(ctx, ex, this);
        // ctx.lineWidth = this.chainWidth;
        ctx.strokeStyle = this.chainStroke;
        ctx.setLineDash(this.chainDash);
        ctx.beginPath();
        drawPoints(ctx, left, connectEndPoints(left, right, -0.25));
        ctx.stroke();
    }
}

export class BandedAnkletPart extends AnkletPart {
    constructor(...data) {
        super({
            legCoverage: 0.6,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        let sp = splitCurve(0.05 + this.startAlongLeg, ex.ankle.in, ex.calf.in);
        const left = sp.right.p1;
        sp = splitCurve(this.legCoverage, left, sp.right.p2);
        const topLeft = sp.left.p2;

        sp = splitCurve(1 - this.startAlongLeg * 0.9, ex.calf.out, ex.ankle.out);
        const right = sp.left.p2;

        sp = splitCurve(1 - this.legCoverage * 0.9, ex.calf.out, right);
        const topRight = sp.right.p1;
        right.cp1 = sp.right.p2.cp1;
        right.cp2 = sp.right.p2.cp2;

        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        drawPoints(ctx, left, topLeft, connectEndPoints(topLeft, topRight, -0.25), right, connectEndPoints(right, left));
        ctx.fill();
    }
}

/**
 * Base Clothing classes
 */
export class Anklet extends Clothing {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.INNER,
        }, ...data);
    }
}


/**
 * Concrete Clothing classes
 */
export class ChainAnklet extends Anklet {
    constructor(...data) {
        super(...data);
    }

    stroke() {
        return "gold";
    }

    fill() {
        return "gold";
    }

    get partPrototypes() {
        return [
            {
                side: this.side,
                Part: ChainAnkletPart,
            },
        ];
    }
}

export class BandedAnklet extends Anklet {
    constructor(...data) {
        super(...data);
    }

    fill() {
        return "gold";
    }

    get partPrototypes() {
        return [
            {
                side: this.side,
                Part: BandedAnkletPart,
            },
        ];
    }
}