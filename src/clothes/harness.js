import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Layer} from "../util/canvas";
import {
    drawPoints,
    extractPoint,
    splitCurve,
    simpleQuadratic,
    adjust,
    interpolateCurve,
    breakPoint,
    clamp,
    drawCircle,
    reverseDrawPoint
} from "drawpoint/dist-esm";
import {
    calcCollarRing, calcCollar,
    ChokerPart,
    CollarPart, setStrokeAndFill
} from "..";
import {Location} from "..";
import {SimpleBeltPart} from "./accessory";

export class StrapPart extends ClothingPart {
    constructor(...data) {
        super({
                strapWidth: 3.,
                thickness : 0.5,
                stroke    : "#111",
                fill      : "#444",
                studColor : "silver",
                studSize  : 0.5,
            },
            ...data);
    }
}

export class ChestStrapPart extends StrapPart {
    constructor(...data) {
        super({
            layer     : Layer.GENITALS,
            loc       : `+${Location.CHEST}`,
            aboveParts: [`parts ${Location.CHEST}`, `decorativeParts ${Location.CHEST}`],
            reflect   : true,
            // aboveSameLayerParts: ["chest"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        Clothes.simpleStrokeFill(ctx, ex, this);

        const {
            midRightTop, midRightBot, midLeftBot, midLeftTop,
            topRightTop, topRightBot, topLeftBot, topLeftTop
        } = calcTopStrap.call(this, ex);

        ctx.beginPath();
        drawPoints(ctx, midRightTop, midRightBot, midLeftBot, midLeftTop, midRightTop);
        drawPoints(ctx, topRightTop, topRightBot, topLeftBot, topLeftTop, topRightTop);
        ctx.stroke();
        ctx.fill();
    }
}

export class UnderChestStrapPart extends StrapPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : `+${Location.CHEST}`,
            aboveParts         : [`parts ${Location.CHEST}`, `decorativeParts ${Location.CHEST}`],
            reflect            : true,
            aboveSameLayerParts: ["chest"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        Clothes.simpleStrokeFill(ctx, ex, this);

        const {underRightTop, underRightBot, underLeftBot, underLeftTop} = calcTopStrap.call(this, ex);

        ctx.beginPath();
        drawPoints(ctx, underRightTop, underRightBot, underLeftBot, underLeftTop, underRightTop);
        ctx.stroke();
        ctx.fill();
    }
}

export class ChestChainStrapPart extends StrapPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : `+${Location.CHEST}`,
            aboveParts         : [`parts ${Location.CHEST}`, `decorativeParts ${Location.CHEST}`],
            reflect            : true,
            aboveSameLayerParts: ["chest"],
            chains             : 13,
            chainDrape         : 1,
            chainThickness     : 1,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        Clothes.simpleStrokeFill(ctx, ex, this);

        let {botOut, botIn} = calcCollar.call(this, ex);
        let t = this.strapWidth * 0.2;
        let sp = splitCurve(clamp(1 - t, 0.2, 0.9), botOut, botIn);
        const topOut = sp.right.p1;
        sp = splitCurve(clamp(t, 0.2, 0.8), sp.right.p1, sp.right.p2);
        const topIn = sp.left.p2;

        botIn = extractPoint(ex.waist);
        if (ex.breast && ex.breast.bot.y < botIn.y) {
            botIn.y = ex.breast.bot.y;
        }
        botOut = adjust(botIn, this.strapWidth * 0.4, this.strapWidth * 0.7);

        const deflection = (ex.breast) ? ex.breast.top.y - ex.chest.nipples.y : 0;
        botIn.cp1 = adjust(topIn, 0, -deflection * 0.9);
        botIn.cp2 = adjust(ex.chest.nipples, deflection * 0.75 + 2, deflection * 0.1 + 3);
        topOut.cp1 = adjust(botIn.cp2, 1, this.strapWidth * 0.8);
        topOut.cp2 = adjust(botIn.cp1, 1, this.strapWidth * 0.8);

        ctx.beginPath();
        drawPoints(ctx, topOut, topIn, botIn, botOut, topOut);
        ctx.fill();
        ctx.stroke();

        // add chains
        const numChains = this.chains;
        const shift = {x: 0, y: 0};
        const points = [];
        const outness = 0.7;
        for (let t = 0.35; t < 0.9; t += 1 / numChains) {
            // drapery gets tighter the closer we are to t = 0.5
            let drapery = clamp(Math.abs(t - 0.56) * 3 + 0.32, 0, 1);
            const flatness = Math.abs(drapery - outness);
            drapery -= (outness - flatness) * 0.2;

            const sp = splitCurve(t, topIn, botIn);
            const out = adjust(sp.left.p2, shift.x, shift.y);
            const center = {x: 0, y: out.y - out.x * this.chainDrape * drapery * (t + 0.2)};
            center.cp1 = {x: out.x * outness, y: center.y * drapery + (out.y + deflection * 0.5) * (1. - drapery)};
            points.push(out, center, breakPoint);
        }

        ctx.lineWidth = this.chainThickness;
        ctx.setLineDash([3, this.chainThickness * 5]);
        ctx.beginPath();
        drawPoints(ctx, ...points);
        ctx.stroke();
    }
}

export class CrotchStrapPart extends StrapPart {
    constructor(...data) {
        super({
            layer        : Layer.MIDRIFT,
            loc          : `+${Location.GROIN}`,
            aboveParts   : [`parts ${Location.GROIN}`, `decorativeParts ${Location.GROIN}`],
            reflect      : true,
            ringColor    : "silver",
            ringWidth    : 3,
            ringThickness: 1,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        // create ring
        const {
            top, right, bot, leftTop, leftCenter, leftBot, rightBot, rightTop,
            botTopRight, botTopLeft, botBotLeft, botBotRight
        } = calcBotStrap.call(this, ex);

        setStrokeAndFill(ctx, {stroke: this.ringColor}, ex);
        ctx.lineWidth = this.ringThickness;

        ctx.beginPath();
        drawPoints(ctx, top, right, bot);
        ctx.stroke();

        Clothes.simpleStrokeFill(ctx, ex, this);
        // create straps connecting to ring
        ctx.beginPath();
        drawPoints(ctx, leftTop, leftCenter, leftBot, rightBot, rightTop, leftTop);
        drawPoints(ctx, botTopRight, botTopLeft, botBotLeft, botBotRight, botTopRight);
        ctx.stroke();
        ctx.fill();

        // studs
        let points = drawStuds.call(this, botTopLeft, botBotLeft, 2);
        points.push(...drawStuds.call(this, leftBot, rightBot, 3, {x: 0, y: this.strapWidth * 0.3}));
        setStrokeAndFill(ctx, {fill: this.studColor}, ex);
        ctx.beginPath();
        drawPoints(ctx, ...points);
        ctx.fill();
    }
}

export class WaistStrapPart extends StrapPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc                : `+${Location.TORSO}`,
            reflect            : true,
            aboveParts         : [`parts ${Location.CHEST}`, `decorativeParts ${Location.CHEST}`],
            // aboveParts: [`parts ${Location.TORSO}`, `decorativeParts ${Location.TORSO}`],
            aboveSameLayerParts: ["chest"],
            chains             : 5,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        Clothes.simpleStrokeFill(ctx, ex, this);

        const {topRight, topLeft, botLeft, botRight, midRight} = calcWaistStrap.call(this, ex);

        ctx.beginPath();
        drawPoints(ctx, topRight, topLeft, botLeft, botRight, midRight, topRight);
        ctx.stroke();
        ctx.fill();

        // studs
        const points = drawStuds.call(this, topLeft, botLeft, this.studs);
        setStrokeAndFill(ctx, {fill: this.studColor}, ex);
        ctx.beginPath();
        drawPoints(ctx, ...points);
        ctx.fill();
    }
}

export class HipChainStrapPart extends StrapPart {
    constructor(...data) {
        super({
            layer              : Layer.MIDRIFT,
            loc                : `+${Location.TORSO}`,
            reflect            : true,
            aboveParts         : [`parts ${Location.TORSO}`, `decorativeParts ${Location.TORSO}`],
            aboveSameLayerParts: [`${Location.TORSO}`],
            studs              : 5,
            ringColor          : "#DAA520",
            ringWidth          : 3,
            ringThickness      : 1,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        // ring
        const center = {x: ex.hip.x * 0.5, y: ex.hip.y - 3};
        const {ring, ringRight, ringLeft} = drawRing(center,
            {strapWidth: this.strapWidth, ringWidth: this.ringWidth, shiftUpRight: 0.15, shiftUpLeft: -0.1});

        // connect right of ring to hips
        const sp = splitCurve(clamp(1 - this.strapWidth * 0.02, 0, 1), ex.waist, ex.hip);
        const outTop = reverseDrawPoint(sp.right.p1, sp.right.p2);
        const outBot = extractPoint(sp.right.p2);

        outBot.cp1 = simpleQuadratic(ringRight.bot, outBot, 0.5, -1.5);
        ringRight.top.cp1 = adjust(outBot.cp1, -0.5, this.strapWidth);

        // connect left of ring to center
        const inTop = {x: -2, y: center.y - 1.5 + this.strapWidth * 0.5};
        const inBot = {x: inTop.x, y: inTop.y - this.strapWidth};
        inBot.cp1 = simpleQuadratic(inTop, inBot, 0.5, -1);

        inTop.cp1 = simpleQuadratic(ringLeft.top, inTop, 0.5, 0.6);
        ringLeft.bot.cp1 = adjust(inTop.cp1, 0, -this.strapWidth);

        // render
        setStrokeAndFill(ctx, {stroke: this.ringColor}, ex);
        ctx.lineWidth = this.ringThickness;
        ctx.beginPath();
        drawPoints(ctx, ring.top, ring.right, ring.bot, ring.left, ring.top);
        ctx.stroke();

        Clothes.simpleStrokeFill(ctx, ex, this);

        ctx.beginPath();
        drawPoints(ctx, ringRight.top, ringRight.center, ringRight.bot, outBot, outTop, ringRight.top);
        drawPoints(ctx, ringLeft.bot, ringLeft.center, ringLeft.top, inTop, inBot, ringLeft.bot);
        ctx.fill();
        ctx.stroke();

        // add chains
        const numChains = this.chains * 0.6;
        const points = [];
        const outness = 0.7;
        for (let t = 0.15; t < 0.7; t += 1 / numChains) {
            const sp = splitCurve(t, ex.hip, ex.thigh.out);
            const out = sp.right.p1;
            out.cp1 = {x: ring.bot.x * (1 - outness) + out.x * outness, y: out.y - out.x * 0.1 * this.chainDrape};
            points.push(ring.bot, out, breakPoint);
        }

        ctx.lineWidth = this.chainThickness;
        ctx.setLineDash([3, this.chainThickness * 5]);
        ctx.beginPath();
        drawPoints(ctx, ...points);
        ctx.stroke();

    }
}

export class LeashPart extends StrapPart {
    constructor(...data) {
        super({
            layer              : Layer.BELOW_HAIR,
            loc                : `+${Location.TORSO}`,
            reflect            : false,
            aboveParts         : [`parts ${Location.CHEST}`, `decorativeParts ${Location.CHEST}`],
            aboveSameLayerParts: ["chest"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        Clothes.simpleStrokeFill(ctx, ex, this);

        const {topRight, topLeft} = calcWaistStrap.call(this, ex);
        // TODO parameterize
        const end = {x: 10, y: 180};
        end.cp1 = {x: topLeft.x + 10, y: topLeft.y - 80};
        end.cp2 = {x: end.x + 5, y: end.y + 20};

        topRight.cp2 = adjust(end.cp1, 2, 0);
        topRight.cp1 = adjust(end.cp2, 1, 0);

        ctx.beginPath();
        drawPoints(ctx, topRight, topLeft, end, topRight);
        ctx.stroke();
        ctx.fill();
    }
}

function drawRing(center, {strapWidth, ringWidth, shiftUpRight, shiftUpLeft}) {
    const [top, right, bot, left] = drawCircle(center, ringWidth);
    const ring = {top, right, bot, left};

    // for connecting strap on right side
    const ringRight = {};
    shiftUpRight = shiftUpRight || 0;
    let t = strapWidth * 0.15 - ringWidth * 0.04;
    let sp = splitCurve(clamp(1 - shiftUpRight - t, 0, 1), top, right);
    ringRight.top = sp.right.p1;
    ringRight.center = sp.right.p2;
    sp = splitCurve(clamp(t - shiftUpRight, 0., 1 - shiftUpRight), right, bot);
    ringRight.bot = sp.left.p2;

    // for connecting strap on left side
    const ringLeft = {};
    shiftUpLeft = shiftUpLeft || 0;
    sp = splitCurve(clamp(1 - t + shiftUpLeft, 0, 1), bot, left);
    ringLeft.bot = sp.right.p1;
    ringLeft.center = sp.right.p2;
    sp = splitCurve(clamp(t + shiftUpLeft, shiftUpLeft, 1), left, top);
    ringLeft.top = sp.left.p2;

    return {ring, ringRight, ringLeft};
}

function drawStuds(start, end, numStuds, shift) {
    shift = shift || {x: 0, y: 0};
    const points = [];
    for (let t = 0.1; t < 1; t += 1 / numStuds) {
        const sp = splitCurve(t, start, end);
        const center = adjust(sp.left.p2, shift.x, shift.y);
        points.push(...drawCircle(center, this.studSize), breakPoint);
    }
    return points;
}

function calcBotStrap(ex) {
    // ring
    const center = adjust(ex.pelvis, 0, -5);
    const {ring, ringRight} = drawRing(center,
        {strapWidth: this.strapWidth, ringWidth: this.ringWidth, shiftUpRight: 0.35});

    const {top, right, bot, left} = ring;
    const leftTop = ringRight.top;
    const leftCenter = ringRight.center;
    const leftBot = ringRight.bot;

    let sp = splitCurve((1 - clamp(this.strapWidth * 0.02, 0, 0.3)), ex.waist, ex.hip);
    let rightTop = sp.right.p1;
    const rightBot = sp.right.p2;
    // since we're going counterclockwise now due to ring on the left side of the strap
    rightTop = reverseDrawPoint(rightTop, rightBot);

    rightBot.cp1 = simpleQuadratic(leftBot, rightBot, 0.5, 2.5);
    leftTop.cp1 = adjust(rightBot.cp1, -0.5, this.strapWidth * 0.7);

    // bot strap
    let t = this.strapWidth * 0.15 - this.ringWidth * 0.04;
    sp = splitCurve(clamp(1 - t, 0.05, 0.95), right, bot);
    const botTopRight = sp.right.p1;
    const botTopLeft = sp.right.p2;
    const botBotLeft = adjust(ex.groin, 0, 0);
    const botBotRight = adjust(botBotLeft, clamp(this.strapWidth * 0.5, 0, 2.5), 0.5);

    return {
        top,
        right,
        bot,
        left,
        leftTop,
        leftCenter,
        leftBot,
        rightBot,
        rightTop,
        botTopRight,
        botTopLeft,
        botBotLeft,
        botBotRight
    };
}

function calcTopStrap(ex) {
    let midRightTop = adjust(extractPoint(ex.armpit), -0.5, -1);
    let midRightBot = adjust(midRightTop, -0.5, -this.strapWidth * 0.5);
    let midLeftBot = {x: 0, y: ex.chest.nipples.y};
    let midLeftTop = {x: 0, y: midLeftBot.y + this.strapWidth};

    let topRightTop = breakPoint;
    let topRightBot = breakPoint;
    let topLeftBot = breakPoint;
    let topLeftTop = breakPoint;

    let underRightTop = breakPoint;
    let underRightBot = breakPoint;
    let underLeftBot = breakPoint;
    let underLeftTop = breakPoint;

    if (!ex.breast || ex.breast.top.y - ex.chest.nipples.y < 2) {
        // handle when breast doesn't exist
        const deflection = midRightTop.y - ex.chest.nipples.y;
        midLeftBot.cp1 = simpleQuadratic(midRightTop, midLeftBot, 0.5, deflection);
        midRightTop.cp1 = adjust(midLeftBot.cp1, 0, this.strapWidth + 1.5);
    } else {
        let sp = splitCurve(0.7, ex.breast.top, ex.breast.tip);

        const deflection = ex.breast.top.y - ex.chest.nipples.y;

        // center strap
        midRightTop = adjust(sp.right.p1, 0, 0.);
        sp = splitCurve(clamp(this.strapWidth * 0.1, 0.05, 0.6), sp.right.p1, sp.right.p2);
        midRightBot = sp.left.p2;

        midLeftBot.cp1 = adjust(ex.chest.nipples, 0, -this.strapWidth * 0.7 - deflection * 0.19);
        midRightTop.cp1 = adjust(midLeftBot.cp1, -0.5, this.strapWidth * 1.2);

        // top strap
        sp = splitCurve(0.3, ex.breast.top, ex.breast.tip);
        topRightTop = adjust(sp.right.p1, 0, 0);
        sp = splitCurve(clamp(this.strapWidth * 0.05, 0.05, 0.4), sp.right.p1, sp.right.p2);
        topRightBot = sp.left.p2;

        topLeftBot = {x: 0, y: midLeftBot.y + this.strapWidth * 0.5};
        topLeftTop = adjust(topLeftBot, 0, this.strapWidth);

        topLeftBot.cp1 = adjust(ex.chest.nipples, 0, this.strapWidth * 0.15 + deflection * 0.6);
        topRightTop.cp1 = adjust(topLeftBot.cp1, -0.5, this.strapWidth * 0.7);

        // under strap
        sp = splitCurve(0.4, ex.breast.tip, ex.breast.bot);
        underRightTop = adjust(sp.right.p1, -0., 0.5);
        underRightBot = adjust(underRightTop, -0.7, -2 - this.strapWidth * 0.2);

        underLeftBot = {x: 0, y: midLeftBot.y - this.strapWidth};
        underLeftTop = adjust(underLeftBot, 0, this.strapWidth);

        underLeftBot.cp1 = simpleQuadratic(underRightBot, underLeftBot, 0.5, deflection * 0.4 + 3);
        underRightTop.cp1 = adjust(underLeftBot.cp1, -0.5, this.strapWidth * 0.7);
    }

    return {
        midRightTop,
        midRightBot,
        midLeftBot,
        midLeftTop,
        topRightTop,
        topRightBot,
        topLeftBot,
        topLeftTop,
        underRightTop,
        underRightBot,
        underLeftBot,
        underLeftTop
    };
}


function calcWaistStrap(ex) {
    const topRing = calcCollarRing.call(this, ex);
    const botRing = calcBotStrap.call(this, ex);

    const t = 1 - this.strapWidth * 0.15 + this.ringWidth * 0.045;
    let sp = splitCurve(clamp(t, 0, 1), topRing.right, topRing.bot);
    const topRight = sp.right.p1;
    const topLeft = sp.right.p2;

    sp = splitCurve(clamp(1 - t, 0, 1), botRing.top, botRing.right);
    let botLeft = adjust(extractPoint(botRing.top), 0, 0);
    const botRight = sp.left.p2;

    // add point in between to make curve more interesting (squeeze in near breasts)
    let query = {x: null, y: ex.chest.nipples.y};
    [sp] = interpolateCurve(botRight, topRight, query);
    const midRight = adjust(sp, -1., 0);

    return {topRight, topLeft, botLeft, botRight, midRight};
}


/**
 * Base Clothing classes
 */
export class Harness extends Clothing {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.OUTER,
        }, ...data);
    }
}

/**
 * Concrete Clothing classes
 */
export class FullLeatherHarness extends Harness {
    constructor(...data) {
        super({}, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: ChestStrapPart,
            },
            {
                side: null,
                Part: UnderChestStrapPart,
            },
            {
                side: null,
                Part: CrotchStrapPart,
            },
            {
                side: null,
                Part: WaistStrapPart,
            },
            {
                side: null,
                Part: LeashPart,
            },
            {
                side: null,
                Part: CollarPart,
            },
            {
                side: null,
                Part: ChokerPart,
            }
        ];
    }
}

export class MetalChainHarness extends Harness {
    constructor(...data) {
        super({
            stroke       : "#DAA520",
            fill         : "#FFD700",
            waistCoverage: 0.8,
        }, ...data);
        this.beltWidth = this.strapWidth;
        this.neckCoverage = this.strapWidth * 0.075;
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: ChestChainStrapPart,
            },
            {
                side: null,
                Part: SimpleBeltPart,
            },
            {
                side: null,
                Part: HipChainStrapPart,
            },
            {
                side: null,
                Part: ChokerPart,
            }
        ];
    }
}
