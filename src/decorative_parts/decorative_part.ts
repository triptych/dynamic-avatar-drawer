import {Layer} from "../util/canvas";
import {BasePart} from "../parts/part";
import { Player } from "../player/player";

/**
 * Base class for non-combat parts for show
 * @memberof module:da
 */
export class DecorativePart extends BasePart {
    constructor(...data) {
        super({
            loc         : null,
            layer       : Layer.BASE,
            reflect     : false,
            coverConceal: [],
            uncoverable : false,
        }, ...data);
    }

    stroke(ctx?: CanvasRenderingContext2D, ex?: any) {
        return "inherit";
    }

    fill(ctx?: CanvasRenderingContext2D, ex?: any) {
        return "inherit";
    }

    // how thick the stroke line should be
    getLineWidth(avatar?: Player) {
        return 1.5;
    }
}

