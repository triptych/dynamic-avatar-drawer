import {addPattern} from "../util/pattern";

export default function loadPatterns() {
    addPattern("sequins",
        "https://i.imgur.com/PoQcggD.jpg");

    addPattern("soft brown fur",
        "https://i.imgur.com/1v9coRk.jpg");
    addPattern("coarse fur",
        "https://i.imgur.com/Ikt1KHU.png");
    addPattern("black leather",
        "https://i.imgur.com/xfA29GM.jpg");
    addPattern("brown leather",
        "https://i.imgur.com/hVWtAl0.jpg");

    addPattern("chain mail 1",
        "https://i.imgur.com/W3iSEWa.jpg");

    addPattern("fishnet",
        "http://i.imgur.com/fU41Daz.png");

    addPattern("lace",
        "https://i.imgur.com/SWkUyef.jpg");

    addPattern("red plaid",
        "https://i.imgur.com/sJGeGWR.jpg");

    addPattern("green camouflage", "http://i.imgur.com/cSQUcjj.png");

    addPattern("camouflage", "https://i.imgur.com/EOyjNND.jpg");

    addPattern("kimono flowers", "https://i.imgur.com/d0RFLql.png");

    addPattern("bandages", "http://i.imgur.com/Om7lYpO.png");

    addPattern("knit cashmere", "https://i.imgur.com/kcyIFUT.jpg");
}
