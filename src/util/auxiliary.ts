import {
    clone,
    splitCurve,
    interpolateCurve,
    adjust,
    getPointOnCurve,
    breakPoint,
    drawCircle, DrawPoint,
    /*
    simpleQuadratic,
    drawPoints,
    extractPoint,
     */
} from "drawpoint/dist-esm";

/**
 takes the length in y axis and compare it with y positions of array of points
 getLimbPoints(
 the highest reference point (aka 0%),
 the lowest reference point (aka 100%),
 percentage of covered area (0-1),
 point1 (the highest one),
 point2,
 etc.
 );
 returns array of points in the covered area
 used for sleeves and pants with letiable length
 */
export function getLimbPoints() {
    function point_between(first, second, totalLength) {
        let distance = first.y - second.y; //distance between two points
        let percent = Math.abs((totalLength - first.y) / distance);//how many percent of distance between two points are covered
        let sp = splitCurve(percent, first, second);
        let temp = sp.left.p2;
        return temp;
    }

    let input_array: any[] = [];
    let output_array: any[] = [];

    const highest_point = arguments[0];
    const lowest_point = arguments[1];
    const coverage = arguments[2];

    const totalLength = highest_point.y - ((highest_point.y - lowest_point.y) * coverage);
    //y position of the last point (position of the first - coverage )

    for (let ii = 3; ii < arguments.length; ii++) {
        if (arguments[ii]) {
            input_array.push(clone(arguments[ii]));
        }
    }

    //no valid points - returns the first one
    if (totalLength > input_array[0].y) {
        output_array[0] = input_array[0];
        //all points valid - returns all
    } else if (totalLength < input_array[input_array.length - 1].y) {
        output_array = input_array;
        //finds the last valid point
    } else {
        for (let ii = 1; ii < input_array.length; ii++) {
            if (totalLength == input_array[ii].y) { //special lucky case
                output_array = input_array.splice(0, (ii + 1));
                break;
            } else if (totalLength > input_array[ii].y) {
                let temp = point_between(input_array[ii - 1], input_array[ii], totalLength);
                output_array = input_array.splice(0, ii);

                output_array[output_array.length] = temp;
                break;
            }
        }
    }
    return output_array;
}

/**
 similar to "getLimbPoints" but needs only the lowest point
 getLimbPointsAbovePoint(
 the lowest reference point
 revert (true/false) - returns reversed array (eg for inner points which needs to be drawn in order from the lowest to the highest)
 point1 (the highest one),
 point2,
 etc.
 );
 used  when you have the lowestmost point on the one side of limb and need to find all the point above it on the other side
 */
export function getLimbPointsAbovePoint() {
    function point_between(first, second, totalLength) {
        let temp = interpolateCurve(first, second, {x: null, y: equalizer_point.y});
        let sp = splitCurve(temp[0].t, first, second);
        return {
            bottom: sp.left.p2,
            second: sp.right.p2
        }
    }

    let input_array: any[] = [];
    let output_array: any[] = [];

    let equalizer_point = arguments[0];
    let totalLength = equalizer_point.y;
    let revert = arguments[1];

    for (let ii = 2; ii < arguments.length; ii++) {
        if (arguments[ii]) {
            input_array.push(clone(arguments[ii]));
        }
    }

    if (totalLength > input_array[0].y) {
        output_array[0] = input_array[0];
    } else if (totalLength < input_array[input_array.length - 1].y) {
        output_array = input_array;
    } else {
        for (let ii = 1; ii < input_array.length; ii++) {
            if (totalLength == input_array[ii].y) {
                output_array = input_array.splice(0, (ii + 1));
                break;
            } else if (totalLength > input_array[ii].y) {
                let bottom;
                if (revert) {
                    let temp = point_between(input_array[ii], input_array[ii - 1], totalLength);
                    bottom = temp.bottom;
                    input_array[ii - 1] = temp.second;
                } else {
                    let temp = point_between(input_array[ii - 1], input_array[ii], totalLength);
                    bottom = temp.bottom;
                }

                output_array = input_array.splice(0, ii);
                output_array.push(bottom);
                break;
            }
        }
    }
    if (revert) {
        output_array.reverse();
    }
    return output_array;
}


/**
 the same as getLimbPoints but returns points that are outside the covered area
 getLimbPointsNegative(
 the highest reference point (aka 0%),
 the lowest reference point (aka 100%),
 percentage of covered area (0-1),
 point1 (the highest one),
 point2,
 etc.
 );
 used for socks
 */
export function getLimbPointsNegative() {
    function point_between(first, second, totalLength) {
        let distance = first.y - second.y; //distance between two points
        let percent = Math.abs((totalLength - first.y) / distance);//how many percent of distance between two points are covered
        let sp = splitCurve(percent, first, second);
        return {
            top   : sp.left.p2,
            second: sp.right.p2
        }
    }

    let input_array : any[] = [];
    let output_array : any[] = [];

    const highest_point = arguments[0];
    const lowest_point = arguments[1];
    const coverage = arguments[2];

    const totalLength = highest_point.y - ((highest_point.y - lowest_point.y) * coverage);
    //y position of the last point (position of the first - coverage )

    for (let ii = 3; ii < arguments.length; ii++) {
        if (arguments[ii]) {
            input_array.push(clone(arguments[ii]));
        }
    }

    //all points valid - return all
    if (totalLength > input_array[0].y) {
        output_array = input_array;
        //no valid points - returns the last one
    } else if (totalLength < input_array[input_array.length - 1].y) {
        output_array[0] = input_array[input_array.length - 1];
        //finds the last valid point
    } else {
        for (let ii = input_array.length - 2; ii >= 0; ii--) {
            if (totalLength < input_array[ii].y) {
                let temp = point_between(input_array[ii], input_array[ii + 1], totalLength);
                let top = temp.top;
                input_array[ii + 1] = temp.second;

                input_array.splice(0, ii + 1);
                output_array = input_array;

                output_array.unshift(top);
                break;
            }
        }
    }
    return output_array;
}


/**
 similar to "getLimbPointsAbovePoint" but needs only the highest point
 getLimbPointsBellowPoint(
 the highest reference point
 revert (true/false) - returns reversed array (eg for inner points which needs to be drawn in order from the lowest to the highest)
 point1 (the highest one),
 point2,
 etc.
 );
 used  when you have the highest point on the one side of limb and need find all the point bellow it on the other side
 */
export function getLimbPointsBellowPoint(control_point, revert) {
    function point_between(first, second, totalLength) {
        let temp = interpolateCurve(first, second, {x: null, y: control_point.y});
        let sp = splitCurve(temp[0].t, first, second);

        return {
            point : sp.left.p2,
            second: sp.right.p2
        }
    }

    let input_array : any[] = [];
    let output_array : any[] = [];
    let totalLength = control_point.y;

    for (let ii = 2; ii < arguments.length; ii++) {
        if (arguments[ii]) {
            input_array.push(clone(arguments[ii]));
        }
    }

    if (totalLength > input_array[0].y) {
        output_array = input_array;
    } else if (totalLength < input_array[input_array.length - 1].y) {
        output_array[0] = input_array[input_array.length - 1];
    } else {
        for (let ii = input_array.length - 2; ii >= 0; ii--) {
            if (totalLength < input_array[ii].y) {

                let temp;
                if (revert) {
                    temp = point_between(input_array[ii + 1], input_array[ii], totalLength);
                } else {
                    temp = point_between(input_array[ii], input_array[ii + 1], totalLength);
                    input_array[ii + 1] = temp.second;
                }

                input_array.splice(0, ii + 1);
                output_array = input_array;

                output_array.unshift(temp.point);
                break;
            }
        }
    }
    if (revert) {
        output_array.reverse();
    }
    return output_array;
}


/**
 findBetween(first value,second value, percent)
 returns value with percent distance between first and second value
 */
export function findBetween(lower, higher, percent = 0.5) {
    if (
        (typeof higher === "object" && typeof higher.x !== undefined && typeof higher.y !== undefined)
        && (typeof lower === "object" && typeof lower.x !== undefined && typeof lower.y !== undefined)
    ) {
        return {
            x: findBetween(lower.x, higher.x, percent),
            y: findBetween(lower.y, higher.y, percent),
        }
    }

    if (typeof higher == "object") {
        console.log("ERROR! The first variable inputed in findBetween() is not a primitive value (ie is object)! ");
    }
    if (typeof lower == "object") {
        console.log("ERROR! The second variable inputed in findBetween() is not a primitive value (ie is object)! ");
    }

    return lower + ((higher - lower) * percent);
}


/**
 gradually straightens curve
 (by moving both control points closer to the centre of the curve)
 */

export function straightenCurve(previous_point, point, percent) {
    let equilibrium = {
        x: findBetween(previous_point.x, point.x, 0.5),
        y: findBetween(previous_point.y, point.y, 0.5)
    };

    if (point.cp1) {
        point.cp1.x = findBetween(point.cp1.x, equilibrium.x, percent);
        point.cp1.y = findBetween(point.cp1.y, equilibrium.y, percent);
    }

    if (point.cp2) {
        point.cp2.x = findBetween(point.cp2.x, equilibrium.x, percent);
        point.cp2.y = findBetween(point.cp2.y, equilibrium.y, percent);
    }
}


/**
 finds point which is intersection of lines AB and CD
 */

export function lineLineIntersection(A, B, C, D) {
    //https://www.geeksforgeeks.org/program-for-point-of-intersection-of-two-lines/

    // Line AB represented as a1x + b1y = c1
    let a1 = B.y - A.y;
    let b1 = A.x - B.x;
    let c1 = a1 * (A.x) + b1 * (A.y);

    // Line CD represented as a2x + b2y = c2
    let a2 = D.y - C.y;
    let b2 = C.x - D.x;
    let c2 = a2 * (C.x) + b2 * (C.y);

    let determinant = a1 * b2 - a2 * b1;

    if (determinant == 0) {
        console.log("Err - lineLineIntersection - they are paralel");
        // The lines are parallel. This is simplified
        return undefined;
    } else {
        let x = (b2 * c1 - b1 * c2) / determinant;
        let y = (a1 * c2 - a2 * c1) / determinant;
        return {x: x, y: y};
    }
}


/**
 finds point which is intersection of bezier curve and line AB
 lineCubicIntersection(startpoint,endpoint,A,B){
 */

export function lineCubicIntersection(start, end, lineA, lineB) {
    //https://www.particleincell.com/2013/cubic-line-intersection/

    //asap - it is in curve.js made it importable
    function solveCubicEquation(a, b, c) {
        const a3 = a / 3;
        const p = (3 * b - a * a) / 3;
        const p3 = p / 3;
        const q = (2 * a * a * a - 9 * a * b + 27 * c) / 27;
        const q2 = q / 2;
        const discriminant = roundToDec(q2 * q2 + p3 * p3 * p3, 8);

        if (discriminant > 0) {
            const sqrtDiscriminant = Math.sqrt(discriminant);
            const u = cubeRoot(-q2 + sqrtDiscriminant);
            const v = cubeRoot(q2 + sqrtDiscriminant);
            const x1 = u - v - a3;
            // ignore other imaginary roots
            return [x1];
        }

        // all roots real (3 in total, 1 single and 1 double)
        if (discriminant === 0) {
            // v = -u
            const u = cubeRoot(-q2);
            // t = u - v, x = t - a/3 = u - v - a/3 = 2u - a/3
            const x1 = 2 * u - a3;
            // conjugate roots produce 1 double root
            const x2 = -u - a3;
            return [x1, x2];
        }
        const r = Math.sqrt(-p3 * p3 * p3);
        let cosphi = -q2 / r;
        if (cosphi < -1) {
            cosphi = -1;
        } else if (cosphi > 1) {
            cosphi = 1;
        }
        const phi = Math.acos(cosphi);
        const commonPrefix = 2 * cubeRoot(r);
        const x1 = commonPrefix * Math.cos(phi / 3) - a3;
        const x2 = commonPrefix * Math.cos((phi + 2 * Math.PI) / 3) - a3;
        const x3 = commonPrefix * Math.cos((phi + 4 * Math.PI) / 3) - a3;
        return [x1, x2, x3];
    }

    function cubeRoot(v) {
        if (v < 0) {
            return -Math.pow(-v, 1 / 3);
        } else {
            return Math.pow(v, 1 / 3);
        }
    }

    function roundToDec(num, numDecimals) {
        return parseFloat(num.toFixed(numDecimals));
    }

    function roundToDecimal(num, dec) {
        return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    }

    let A = lineB.y - lineA.y;	    //A=y2-y1
    let B = lineA.x - lineB.x;	    //B=x1-x2
    let C = lineA.x * (lineA.y - lineB.y) + lineA.y * (lineB.x + lineA.x);

    function bezierCoeffs(P0, P1, P2, P3) {
        let Z = Array();
        Z[0] = -P0 + 3 * P1 + -3 * P2 + P3;
        Z[1] = 3 * P0 - 6 * P1 + 3 * P2;
        Z[2] = -3 * P0 + 3 * P1;
        Z[3] = P0;
        return Z;
    }

    let bx = bezierCoeffs(start.x, end.cp1.x, end.cp2.x, end.x);
    let by = bezierCoeffs(start.y, end.cp1.y, end.cp2.y, end.y);

    let P = Array();
    P[0] = A * bx[0] + B * by[0];		/*t^3*/
    P[1] = (A * bx[1] + B * by[1]) / P[0];	/*t^2*/
    P[2] = (A * bx[2] + B * by[2]) / P[0];	/*t*/
    P[3] = (A * bx[3] + B * by[3] + C) / P[0];	/*1*/

    let roots = solveCubicEquation(P[1], P[2], P[3]);


    let root = 0;
    let t = root;
    for (let i = 0; i < roots.length; i++) {
        root = roundToDecimal(roots[i], 15);
        if (root >= 0 && root <= 1) {
            t = root;
        }
    }

    let X = bx[0] * t * t * t + bx[1] * t * t + bx[2] * t + bx[3];
    let Y = by[0] * t * t * t + by[1] * t * t + by[2] * t + by[3];

    //ASAP - ASUMES THERE IS ONLY ONE VALID POINT
    return {x: X, y: Y};
}


/**
 finds if point C is located on line defined by points A and B
 (returns 0 if it is)
 */
//TODO??
export function pointLineIntersection(A, B, C) {
    function lineEquation(A, B) {
        let m = (
            (B.y - A.y) / (B.x - A.x)
        );

        let b = (
            A.y - (m * A.x)
        );

        //y = mx + b
        return {
            m: m,
            b: b,
        };
    }

    let temp = lineEquation(A, B);
    //y - mx - b = 0;
    return (C.y - (temp.m * C.x) - temp.b);
}


/**
 returns points for drawing lacing between two curves
 getLacingPoints(
 startpoint of the inner curve
 endpoint of the inner curve
 startpoint of outer
 endpoint of outer
 number of crossings
 adjustment (to not have lace hole straight on the curve)
 );
 */
export function getLacingPoints(innerStart, innerEnd, outerStart, outerEnd, count, adjustment = 0) {
    count = Math.abs(Math.round(count));
    count++;
    let inner : DrawPoint[] = [];
    let outer : DrawPoint[] = [];

    let step = 1 / count;

    for (let ii = 0; (ii * step) <= 1; ii++) {
        if (ii % 2 != 0) {
            inner[ii] = adjust(getPointOnCurve(step * ii, innerStart, innerEnd), adjustment, 0);
            outer[ii] = adjust(getPointOnCurve(step * ii, outerStart, outerEnd), -adjustment, 0);
        } else {
            inner[ii] = adjust(getPointOnCurve(step * ii, outerStart, outerEnd), -adjustment, 0);
            outer[ii] = adjust(getPointOnCurve(step * ii, innerStart, innerEnd), adjustment, 0);
        }
    }

    return {inner: inner, outer: outer};
}


/**
 first finds a point T on the line between points "A" and "B" with distance from "A" "percent"*AB
 then returns point C perpendicular to line AB with distance from the point T "distance"

 C
 |
 A---T----B

 perpendicularPoint(
 A - fist point {x,y}
 B - second point {x,y}
 percent - percentage of distance AB (values 0-1)
 distance - perpendicular distance from a point T on line AB and the resulting point C
 )
 returns C
 */
export function perpendicularPoint(A, B, percent, distance) {
    let T = findBetween(A, B, percent);
    let angle = cartesian2polar(B, A).theta + (Math.PI / 2);
    return polar2cartesian(distance, angle, T)

}

/**
 transfers point from Cartesian coordinates to Polar
 */
export function cartesian2polar(point, origin) {
    if (!origin) {
        origin = {};
    }
    if (!origin.x) {
        origin.x = 0;
    }
    if (!origin.y) {
        origin.y = 0;
    }

    const a = (point.x - origin.x);
    const b = (point.y - origin.y);
    return {
        r    : Math.sqrt(
            Math.pow(a, 2) + Math.pow(b, 2)
        ),
        theta: Math.atan2(b, a)
    }
}

/**
 transfers point from Polar coordinates to Cartesian
 theta is in radians
 */
export function polar2cartesian(r, theta, origin) {
    if (!origin) {
        origin = {};
    }
    if (!origin.x) {
        origin.x = 0;
    }
    if (!origin.y) {
        origin.y = 0;
    }

    return {
        x: r * Math.cos(theta) + origin.x,
        y: r * Math.sin(theta) + origin.y
    }
}

/**
 copies control points from the first curve to the second curve,
 adjusted by size difference and rotation between both curves
 copyCurve(
 start of the original fist curve,
 end of the original curve (with control points),
 start of the second curve (or at this point only a line before cps are added)
 end of the second curve
 )
 */

//TODO NOT SURE IF WORKS PROPERLY 
export function copyCurve(START, END, start, end) {
    //find scale
    const STARTEND = cartesian2polar(END, START).r;
    const startend = cartesian2polar(end, start).r;
    const scale = startend / STARTEND;

    //centers of curves
    const T = findBetween(END, START);
    const t = findBetween(end, start);

    if (END.cp1) {
        //rotation
        let {r, theta} = cartesian2polar(END.cp1, T);
        theta -= cartesian2polar(END, T).theta;
        theta += cartesian2polar(end, t).theta;

        end.cp1 = polar2cartesian(r * scale, theta, t)
    }

    if (END.cp2) {
        //rotation
        let {r, theta} = cartesian2polar(END.cp2, T);
        theta -= cartesian2polar(END, T).theta;
        theta += cartesian2polar(end, t).theta;

        end.cp2 = polar2cartesian(r * scale, theta, t)
    }
}

/**
 draws spiral
 drawSpiral(
 center around which the spiral spirals
 outermost startpoint
 number of loops around the center
 direction: -1 clockwise; 1 counterclockwise //TO DO - maybe it is the other way around
 )
 */

export function drawSpiral(center, start, loops, direction = 1) {
    //based on https://subscription.packtpub.com/book/web_development/9781849691369/1/ch01lvl1sec16/drawing-a-spiral
    const iterations = 50; //complete circle consist of iterations
    direction = direction >= 0 ? 1 : -1;
    let points : DrawPoint[] = [];

    let {r, theta} = cartesian2polar(start, center);
    let step = r / (iterations * loops);

    points[points.length] = polar2cartesian(r, theta, center);

    for (let i = 0; i < (iterations * loops); i++) {
        r -= step;
        theta -= (Math.PI * 2) / iterations * direction;

        points[points.length] = polar2cartesian(r, theta, center);
    }

    return points;
}

/**
 drawStar(
 center {x,y}
 number of spikes
 outerRadius
 innerRadius
 up (boolean) - the first spike should be facing upwards (false - downwards)
 outer -  0 = nothing; 1 = there should be a circle drawn around the star; -1 = there should be a polygon drawn around the star;
 inner - the same thing as outer but inside the star
 )
 (the last three parameters are there to enable drawing nice pentagrams)
 */
export function drawStar(center, spikes, outerRadius, innerRadius, up = true, outer = 0, inner = 0) {
    let starpoints : DrawPoint[] = [];
    let rot = Math.PI / 2 * 3; //rotation???
    let step = Math.PI / spikes;

    let direction = up ? -1 : 1;

    //stars spikes
    for (let i = 0; i < spikes; i++) {
        starpoints[starpoints.length] = {
            x: center.x + (Math.cos(rot) * outerRadius * direction),
            y: center.y + (Math.sin(rot) * outerRadius * direction)
        };
        rot += step;
        starpoints[starpoints.length] = {
            x: center.x + (Math.cos(rot) * innerRadius * direction),
            y: center.y + (Math.sin(rot) * innerRadius * direction)
        };
        rot += step;
    }
    starpoints[starpoints.length] = {
        x: center.x,
        y: center.y - (outerRadius * direction)
    };

    //outer circle/polygon
    if (outer > 0) {
        starpoints[starpoints.length] = breakPoint;
        starpoints = starpoints.concat(drawCircle({x: center.x, y: center.y}, outerRadius));
    } else if (outer < 0) {
        starpoints[starpoints.length] = breakPoint;
        for (var i = 0; i <= (spikes * 2);) {
            starpoints[starpoints.length] = starpoints[i];
            i++;
            i++;
        }
        starpoints[starpoints.length] = starpoints[0];
    }

    //inner circle/polygon
    if (inner > 0) {
        starpoints[starpoints.length] = breakPoint;
        starpoints = starpoints.concat(drawCircle({x: center.x, y: center.y}, innerRadius));
    } else if (inner < 0) {
        starpoints[starpoints.length] = breakPoint;
        for (var i = 1; i <= (spikes * 2);) {
            starpoints[starpoints.length] = starpoints[i];
            i++;
            i++;
        }
        starpoints[starpoints.length] = starpoints[1];
    }

    return starpoints
}