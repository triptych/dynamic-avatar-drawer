import {getLoadedPattern} from "./pattern";
import {extractUnmodifiedLocation} from "./part";
import {diff, norm, splitCurve, none} from "drawpoint/dist-esm";
import {Clothing} from "../clothes/clothing";

/**
 * conversions between canvas units (cu) to centimeters (cm) and inches (in)
 */
export function incu(inches) {
    return cmcu(inches * 2.54);
}

export function cmcu(cm) {
    return cm * 2;
}

export function cucm(cu) {
    return cu / 2;
}

export function convertPointsToCanvasUnits(points) {
    if (Array.isArray(points)) {
        for (let i = 0; i < points.length; ++i) {
            convertPointsToCanvasUnits(points[i]);
        }
        return;
    }
    if (typeof points !== "object") {
        return;
    }  // can't hold any draw points

    const names = Object.getOwnPropertyNames(points);
    for (let n = 0; n < names.length; ++n) {
        let name = names[n];
        if (name === "x" || name === "y") {
            points[name] = cmcu(points[name]);
        } else {
            convertPointsToCanvasUnits(points[name]);
        }
    }
}

/**
 * Set stroke and fill for given context
 * @memberof module:da
 * @param {CanvasRenderingContext2D} ctx
 * @param {Part} part Some kind of Part object
 * @param {object} ex Export from draw
 */
export function setStrokeAndFill(ctx, part, ex) {
    let stroke = getLoadedPattern.call(part, part.stroke, ctx, ex);

    if (stroke === "inherit") {
        ctx.strokeStyle = ex.baseStroke;
    } else {
        ctx.strokeStyle = stroke;
    }

    let fill = getLoadedPattern.call(part, part.fill, ctx, ex);

    if (fill === "inherit") {
        ctx.fillStyle = ex.baseFill;
    } else {
        ctx.fillStyle = fill;
    }
}

/**
 * Inherit the stroke style of its parent part (must have this bound to a part)
 * @param ctx
 * @param ex
 * @returns {*}
 */
export function inheritStroke(ctx, ex) {
    const myLoc = extractUnmodifiedLocation(this.loc);
    // first check if there's clothing here
    const coveringClothing = ex.avatar.getClothingInLocation.call(ex.avatar, myLoc);
    let topClothing : null | Clothing = null;
    coveringClothing.forEach((clothing) => {
        if (topClothing === null || clothing.clothingLayer > topClothing!.clothingLayer) {
            topClothing = clothing;
        }
    });

    // if covering with clothing use it, else use parent body part's colour
    if (topClothing) {
        const realClothing = topClothing as Clothing;
        if (typeof realClothing.stroke === "function") {
            return realClothing.stroke(ctx, ex);
        }
        return realClothing.stroke;
    } else {
        const parentPart = ex.avatar.getPartInLocation.call(ex.avatar, myLoc);
        if (parentPart) {
            if (typeof parentPart.stroke === "function") {
                const parentStroke = parentPart.stroke(ctx, ex);
                if (parentStroke !== none) {
                    return parentStroke;
                }
            }
            if (parentPart.stroke !== none) {
                return parentPart.stroke;
            }
        }
        return ex.baseStroke;
    }
}

export function inheritFill(ctx, ex) {
    const parentPart = ex.avatar.getPartInLocation(this.loc);
    if (parentPart) {
        if (typeof parentPart.fill === "function") {
            return parentPart.fill(ctx, ex);
        }
        return parentPart.fill;
    }
    return ex.baseFill;
}

export function requirePart(partName, ex) {
    if (ex.hasOwnProperty(partName) === false) {
        throw new Error(`Trying to draw ${partName} but it hasn't been defined yet`);
    }
}

export function dist(pointA, pointB) {
    const myDiff = diff(pointA, pointB);
    return norm(myDiff);
}

/**
 * DEPRECATED function, use simpleQuadratic instead
 */
export function averageQuadratic(p1, p2, t, dx, dy, st, et) {
    // draw a smooth quadratic curve with the control point t along the straight line from p1 to p2
    // disturbed with dx and dy if applicable
    if (!t) {
        t = 0.5;
    }
    if (!dx) {
        dx = 0;
    }
    if (!dy) {
        dy = 0;
    }
    let cp1 = {
        x: p1.x * t + p2.x * (1 - t) + dx,
        y: p1.y * t + p2.y * (1 - t) + dy
    };
    // start time not the default value of 0
    if (st) {
        const pend = p2;
        pend.cp1 = cp1;
        const sp = splitCurve(st, p1, pend)!;
        cp1 = sp.right.p2.cp1!;
    }
    if (et) {
        const pend = p2;
        pend.cp1 = cp1;
        const sp = splitCurve(et, p1, pend)!;
        cp1 = sp.left.p2.cp1!;
    }
    return cp1;
}
