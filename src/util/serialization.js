/*eslint no-unused-vars: ["error", {"varsIgnorePattern": "\$[^\b]*\$"}]*/
import deepEqual from "deep-equal";

// Utility method to allow users to easily wrap their code in the revive wrapper.
if (!JSON.reviveWrapper) {
    Object.defineProperty(JSON, "reviveWrapper", {
        configurable: true,
        writable    : true,

        value: function (code, data) {
            if (typeof code !== "string") {
                throw new TypeError("JSON.reviveWrapper code parameter must be a string");
            }

            return ["(revive:eval)", [code, data]];
        }
    });
}

// Serialize data into a JSON-encoded string.
export function serialize(value) {
    return JSON.stringify(value);
}

export function deserialize(text) {
    return JSON.parse(text, function (key, val) {
        let value = val;

        // Attempt to revive wrapped values.
        if (Array.isArray(value) && value.length === 2 && value[0] === "(revive:eval)") {
            const $ReviveData$ = value[1][1];
            value = eval(value[1][0]); // eslint-disable-line no-eval
        }

        return value;
    });
}

function cloneObjWithout(obj, keysToExclude) {
    const data = Object.create(Object.getPrototypeOf(obj));
    Object.keys(obj).forEach(function (key) {
        if (!keysToExclude.includes(key)) {
            const value = obj[key];
            data[key] = (value !== null && typeof (value) === "object" && !Array.isArray(value)) ?
                cloneObjWithout(value, keysToExclude) : value;
        }
    });
    return data;
}

// parts shouldn't
const serializationKeysExclude = ["_owner", "shadingParts", "clothingParts"];

function extendSerializability(globalClassIdentifier) {
    const classConstructor = eval(globalClassIdentifier);
    Object.defineProperties(classConstructor.prototype, {
        /*
         Returns a simple object encapsulating our own data properties.
         */
        _getData: {
            value: function () {
                // compare against a default constructed version of this object and only include what's different
                const defaultObj = cloneObjWithout(new classConstructor(), serializationKeysExclude);
                const thisObj = cloneObjWithout(this, serializationKeysExclude);

                const data = {};
                Object.keys(thisObj).forEach(function (key) {
                    if (!defaultObj.hasOwnProperty(key) || !deepEqual(defaultObj[key], thisObj[key])) {
                        data[key] = thisObj[key];
                    }
                });

                return data;
            }
        },

        /*
         Allows the object to be properly cloned from passage to passage.
         */
        clone: {
            value: function () {
                return new classConstructor(this._getData());
            }
        },

        /*
         Allows the object to be properly restored from serializations.
         */
        toJSON: {
            value: function () {
                return JSON.reviveWrapper(
                    "(new " + globalClassIdentifier + "($ReviveData$))",
                    this._getData()
                );
            }
        }
    });
}

export function loadSerialization() {
    /*
     Merge the requisite properties onto instantiable classes (only).
     */
    const globalClassNames = [];
    const namespace = window.da;

    for (let className in namespace) {
        if (namespace.hasOwnProperty(className) === false) {
            continue;
        }
        const exportedClass = namespace[className];

        if (typeof exportedClass === "function") {
            // heuristic of constructors always having a capital letter...
            if (className.length > 1 &&
                className.charAt(0) === className.charAt(0).toUpperCase()) {
                globalClassNames.push(className);
            }
        }
    }

    // Add entries for the rest of DAD's instantiable classes here.
    globalClassNames.forEach(function (className) {
        extendSerializability("da." + className);
    });
}
