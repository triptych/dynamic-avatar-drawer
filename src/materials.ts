/**
 * Where common materials go, where each can act as style overrides, usually
 * by having a fill and stroke property or method
 * @namespace Materials
 * @memberof module:da
 */
export const Materials = {
    brownFur: Object.freeze({
        stroke      : "#663300",
        fill        : "#ac7339",
        // fur parts can be tucked under
        coverConceal: ["this"],
    }),

    sheerFabric: Object.freeze({
        stroke: "#000",
        fill  : "rgba(0,0,0,0.8)",
    }),
};
