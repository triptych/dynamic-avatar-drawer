[live tester / playground](http://perplexedpeach.gitlab.io/dynamic-avatar-drawer/dist/test.html)

[documentation / updates](http://perplexedpeach.gitlab.io/dynamic-avatar-drawer/)

Function
===
Model and View for characters in Javascript-enabled games and stories.

It provides:
 - procedural drawing of characters and clothing
 - inventory management and clothing layering
 - facial expression system
 - mixing body parts (e.g. elf ears on human head)
 - missing body parts (e.g. hand was cut off)
 - smooth animation for transformations as a result of stat changes
 - interface for defining custom stats and mapping those to appearance dimensions

Usage
===
There are 3 ways to use this library:
1. Include the packaged `da.js` file (under `dist/da.js`) in your project and call methods on the global `da` module 
(simplest use case for simplest projects)
2. Import JavaScript ES6 modules (under `dist-esm`) in your modules, then bundle using a tool like webpack
(suitable for larger JavaScript projects)
3. Import TypeScript modules (under `src`) in your TypeScript code, then transpile with `tsc` and bundle using a tool like webpack
(recommended; suitable for larger TypeScript projects; also gives you typing information)


See top for link to usage and content creation guide.
For an API reference, see the `ref` directory of this repository or the link above.

Use the playground to generate live code and see their effects.

For development, you'll need to install some dependencies. First install the latest version of 
[node and npm](https://nodejs.org/en/download/)

Then open a console, navigate to the project root directory, then run
```bash
npm install
```

For development, run 
```bash
npm run start
```

To start a webdev server that will launch the demo, listen for file changes, recompile
on file changes, and refresh the demo after it's done recompiling.

For building `public/da.js` for release/production, run
```bash
npm run build
```

Local website build
===
Set up dependencies

1. install the latest version of ruby
2. install rubydevkit and point to the previous installation of ruby
3. navigate to portfolio root directory with git bash
4. `gem install bundler`
5. `bundle install`

Run local server
1. `bundle exec jekyll serve`

Writing documentation
===
1. write content in markdown inside the `src` directory (see examples in there)
2. `node doc_src/build.js <path to markdown source inside doc_src/>`
    - will automatically generate and move the resulting HTML file into the `doc` directory
    
### Markdown language extensions
Put asides in a block of text starting with `>>` or `>>>` for more important notes, like:

```
>> This is an aside.
This is still part of the aside.

No longer part of the aside
```
